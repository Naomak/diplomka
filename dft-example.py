#!/usr/bin/python2.7

from os import sys, path
import cv2

import numpy as np
from matplotlib import pyplot as plt

from scipy import log10
from scipy import fftpack
from scipy import random

from numpy import fft 
import utils

def runFFT():

    BASIC = 1
    COMLICATE = 2

    # signal_type = BASIC
    signal_type = COMLICATE

    if signal_type == BASIC:
        # BASIC signal ---------------
        f = 5.          # frekvence
        t = 20           # doba 
        dx = 80        # vzorkovaci frekvence
        n = dx * t      # pocet vzorku
        ampl = 2.0
        x = np.linspace(0, 20, n)
        y = ampl * np.sin(2 * np.pi * f * x)

        Ff = fftpack.fft(y)
        Fx = fftpack.fftfreq(n, d=1/float(dx)) 
    elif signal_type == COMLICATE:
        #  COMPLICATE signal --------
        n = 4000
        x = np.linspace(0, 5, n)
        # acc = lambda x: 7*np.sin(2*np.pi*2.0*x) + 4*np.sin(2*np.pi*8.0*x) + 2*random.random(len(x))
        acc = lambda x: 7*np.sin(2*np.pi*2.0*x) + 4*np.sin(2*np.pi*8.0*x)

        y = acc(x)  # signal

        Ff = fftpack.fft(y)
        Fx = fftpack.fftfreq(n, x[1]-x[0])

    # Fk = fft.fft(fx) # Fourier coefficients (divided by n)
    # nu = fft.fftfreq(n, dx) # Natural frequencies
    # Fk = fft.fftshift(Fk) # Shift zero freq to center
    # nu = fft.fftshift(nu) # Shift zero freq to center


    plt.suptitle('Signal 7*sin(2pi*2*t) + 4*sin(2pi*8.0*t) a jeho spektrum')
    ax1 = plt.subplot(211)
    plt.xlabel(u'Cas')
    plt.ylabel('')
    plt.plot(x, y)
    plt.grid(True)


    # plt.subplot(312, sharex=ax1)
    # plt.xlabel(u'freq')
    # plt.ylabel('Abs(magnitude)')
    # plt.plot(Fx,abs(Ff))
    # plt.grid(True)

    ax = plt.subplot(212)
    plt.xlabel('frekvence')
    plt.ylabel('abs(F * 2 / N)')
    plt.plot(Fx,abs(Ff * 2 / n))
    # ax.axvline(2, linestyle='..', color='k')
    ax.set_xticks(range(-10,11,2), minor=False)
    plt.grid(True)

    # plt.subplot(514, sharex=ax1)
    # plt.xlabel(u'freq')
    # plt.ylabel('power(abs norm * 2 / N)')
    # plt.plot(Fx,abs(Ff * 2 / n)**2)
    # plt.grid(True)
    

    ####################
    # plt.subplot(512, sharex=ax1)
    # plt.xlabel(u'freq')
    # plt.ylabel('log10(abs)')
    # plt.plot(Fx,log10(abs(Ff)) )
    # plt.grid(True)

    # plt.subplot(513, sharex=ax1)
    # plt.xlabel(u'freq')
    # plt.ylabel('log10(power)')
    # plt.plot(Fx,log10(abs(Ff)**2) )
    # plt.grid(True)

    # plt.subplot(514, sharex=ax1)
    # plt.xlabel(u'freq')
    # plt.ylabel('20*log10(power)')
    # plt.plot(Fx,20*log10(abs(Ff)**2) )
    # plt.grid(True)

    # plt.subplot(515, sharex=ax1)
    # plt.xlabel(u'freq')
    # plt.ylabel('20 * log10(abs)')
    # plt.plot(Fx,20*log10(abs(Ff)) )
    # plt.grid(True)

    ####################


    plt.show()

def test():
    a = np.array([[5, 25], [30, 48]], dtype=np.int32)
    b = np.array([[10, 90], [69, 60]], dtype=np.int32)
    c = a + b
    # import ipdb; ipdb.set_trace();



    print a
    print b
    print c
    d = np.empty_like(a)
    # import ipdb; ipdb.set_trace();
    for x in xrange(a.shape[0]):
        for y in xrange(a.shape[1]):
            tmp = a[x,y] + b[x,y]
            if tmp <=100:
                d[x,y] = tmp
            else:
                d[x,y] = a[x,y]

    print d

def rewrite_vid():
    path = '/home/goffrey/Skola/Diplomka/src/testy/drive/peta_conv_1.avi'

    

    video_cap = cv2.VideoCapture(path)

    # set into self. width, height, frame_cnt, fps
    fps = int(video_cap.get(cv2.cv.CV_CAP_PROP_FPS))
    width = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
    height = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
    frame_cnt = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))

    print 'Video params:'
    print 'FPS: ', `fps`
    print 'Width: ', `width`
    print 'Height: ', `height`
    print 'Frame count: ', `frame_cnt`


        
    fourcc = cv2.cv.CV_FOURCC('M', 'J', 'P', 'G')
    vid_writer = cv2.VideoWriter('/home/goffrey/Skola/Diplomka/src/testy/drive/peta_shorter.avi', fourcc, fps, (width, height), isColor=True)

    for i in range(600):
        rval, frame = video_cap.read()
        vid_writer.write(frame)

    video_cap.release()
    vid_writer.release()



def mean():

    ar = np.array([ [   [135, 169, 193], [122, 159, 183], [120, 157,181]],

                     [  [123, 160, 182], [119, 156, 178], [126, 163, 185]],

                     [  [110, 147, 169], [119, 156, 178], [124, 161, 183]]])


    return utils.get_area_mean(((0, 0), (3, 3)), ar)


def graph_bolt_altman():

    # Rozdily ABS
    # y = [21.66,
    #     52.75,
    #     28.91,
    #     30.
    #     23.8,
    #     6.83,
    #     24.08,
    #     2.73,
    #     2.73,
    #     0.33,
    #     13,
    #     1,
    #     45,
    #     13.33,
    #     9.85,
    # ]

    # Rozdily
    y_ruka = [5,
        49,
        38,
        36,
        26,
        11,
        31,
        10,
    ]
    y_stabilni = [
        0,
        0,
        -2,
        11,
        -4,
        50,        
        -4,
        33,
        3,
        1,
        ]

    x_ruka = [
        85,
        109,
        88,
        66,
        66,
        71,
        71,
        90,
    ]
    x_stabilni = [
        60,
        60,
        68,
        71,
        76,
        80,
        
        56,
        73,
        83,
        51,

    ]

    ax = plt.plot(x_ruka, y_ruka, 'bo', x_stabilni, y_stabilni, 'ro')
    # plt.title(title)
    plt.xlabel('Puls')
    plt.ylabel(u'Rozd\xedl srde\u010dn\xedho pulsu $SP_{ref} - SP_{detected}$')

    plt.axhline(y=16.3, color='r', linestyle='dashed')
    plt.text(100, 16.8, u'Pr\u016fm\u011br')

    plt.axhline(y=64.29, color='b', linestyle='dashed')
    plt.text(100, 64.79, u'Pr\u016fm\u011br + 1,96SO')

    plt.axhline(y=-31.63, color='b', linestyle='dashed')
    plt.text(100, -31.13, u'Pr\u016fm\u011br - 1,96SO')
    # fps = 30
    # signal_len = len(xaxis)
    tics = np.array([-50,-40,-30,-20,-10,0,10,20,30,40,50, 60,70])
    plt.yticks(tics)

    plt.grid(True)
    plt.show()

def graph_dpuble_bolt_altman():

    x_ruka = [85, 88, 71, 72, 90]
    x_stabil = [80,73,83]

    y_ruka_new = [5, -2, 1, 2, 15]
    y_stab_new = [20, -7, 3]

    y_ruka_old = [5, 38, 11, 31, 10]
    y_stab_old = [50, 33, 3]

    ax_old = plt.subplot(121)
    plt.plot(x_ruka, y_ruka_old, 'bo', x_stabil, y_stab_old, 'ro')
    # plt.title(title)
    plt.xlabel('Puls')
    plt.ylabel(u'Rozd\xedl srde\u010dn\xedho pulsu $SP_{ref} - SP_{detected}$')

    plt.axhline(y=22.625, color='r', linestyle='dashed')
    plt.text(95, 23.125, u'Pr\u016fm\u011br')

    plt.axhline(y=70.36, color='b', linestyle='dashed')
    plt.text(95, 70.86, u'Pr\u016fm\u011br + 1,96SO')

    plt.axhline(y=-25.11, color='b', linestyle='dashed')
    plt.text(95, -24.61, u'Pr\u016fm\u011br - 1,96SO')

    plt.axhline(y=0, color='k')
    # fps = 30
    # signal_len = len(xaxis)
    tics = np.array([-50,-40,-30,-20,-10,0,10,20,30,40,50, 60,70])
    plt.yticks(tics)
    plt.grid(True)

    ##################################
    ax_new = plt.subplot(122, sharey=ax_old, sharex=ax_old)
    plt.plot(x_ruka, y_ruka_new, 'bo', x_stabil, y_stab_new, 'ro')

    plt.axhline(y=4.625, color='r', linestyle='dashed')
    plt.text(95, 5.125, u'Pr\u016fm\u011br')

    plt.axhline(y=20.815, color='b', linestyle='dashed')
    plt.text(95, 21.315, u'Pr\u016fm\u011br + 1,96SO')

    plt.axhline(y=-11.565, color='b', linestyle='dashed')
    plt.text(95, -11.065, u'Pr\u016fm\u011br - 1,96SO')

    plt.axhline(y=0, color='k')

    tics = np.array([-50,-40,-30,-20,-10,0,10,20,30,40,50, 60,70])
    plt.yticks(tics)

    plt.grid(True)
    plt.show()

if __name__ == '__main__':
    # runFFT()
    # sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    # print mean()
    # test()
    # rewrite_vid()

    # --- GRAPHS ---
    # graph_bolt_altman()

    graph_dpuble_bolt_altman()

