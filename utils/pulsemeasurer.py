
import numpy as np
import numpy.ma as ma
from scipy import signal as sci_signal
from scipy import log10

from graphs import Graphs
from utils import Utils

class PulseMeasurer():
    def __init__(self):
        self.fps = 0
        self.time_window = 0.0
        self.utils = Utils()
        self.graphs = Graphs()

    def measure_pulse(self, signal, fps, color_type, time_window):
        '''
        Parameters:
        signal          - 3 channel color signal
        fps             - used to properly count 
        color_type      - ycc / bgr to specify which channel to compute
        time_window     - window in witch the pulse (the highest value will be searched for)
                        - in seconds (like 1.4)
        '''
        print 'Measure pulse'

        self.fps = fps
        self.time_window = time_window
        
        # suppose signal is bgr - selecting green value
        if color_type == 'ycc':
            channel = 0
        else:
            channel = 1

        pulse = self.do_measurement(signal[:, channel])

        return pulse

    def do_measurement(self, orig_signal):
        '''
        Detect local maxima, and count the number of them, computing bpm based on signal length and fps.
        For proper detecting on the borders, signal is prolonged on both ends by 2 times fps.
        '''

        show_graphs = False         # hardcoded graph control
        show_window_detail = False

        prolong_len = self.fps * 2
        prolongue = True
        if prolongue:
            signal = np.ndarray( (orig_signal.shape[0] + (2*prolong_len)), dtype=orig_signal.dtype)
            signal[:prolong_len] = orig_signal[0]
            signal[prolong_len:orig_signal.shape[0]+prolong_len] = orig_signal[:]
            signal[orig_signal.shape[0]+prolong_len:] = orig_signal[-1]
        else:
            signal = orig_signal

        window = int(self.time_window * self.fps)  # length of the window in frames
        pulse_signal = np.zeros(orig_signal.shape[0], dtype=np.uint8)

        # counting number of peaks inside original signal length
        peaks = sci_signal.find_peaks_cwt(signal, np.arange(1, window))
        # move back position of peaks, because prolong part is meant to be croped
        peaks = np.array(map(lambda (x): x-prolong_len, peaks))
        valid_peaks = peaks[(peaks>=0) & (peaks < orig_signal.shape[0])]
        print 'peaks: ', `valid_peaks`
        print 'diffs: ', `np.diff(valid_peaks)`
        print 'meadian: ', `np.median(np.diff(valid_peaks))`


        # note positions for graph printing
        for peak_ind in valid_peaks:
            pulse_signal[peak_ind] = 1


        bpm = 60.0 * float(self.fps) / np.median(np.diff(valid_peaks))
        print '|----------------------------------'
        print '| MEDIAN peak distance: | {0:.2f}'.format(bpm)
        print '|----------------------------------'
        if show_graphs:
            self.graphs.show_2_signals(orig_signal, pulse_signal, self.fps)

        ###################
        # count bpm from mean signal spektrum
        signal_len = orig_signal.shape[0]
        F, freq = self.utils.fft_on_signal(orig_signal, self.fps)
        # find frequency index of lower frequency border
        low_index = (np.abs(freq[:signal_len/2] - 0.5)).argmin()
        high_index = (np.abs(freq[:signal_len/2] - 1.5)).argmin()
        max_index = log10( abs(F[low_index:high_index] * 2 / signal_len)).argmax() + low_index

        bpm = freq[max_index] * 60.
        print '| SPEKTRUM-max pulse:    | {0:.2f}'.format(bpm)
        print '|----------------------------------'
        if show_graphs:
            self.graphs.signal_1ch_fft(orig_signal, self.fps)

        # check count spectrum and find max for window with step
        window_len = 6 * self.fps
        step = self.fps
        start = 0
        stop = window_len

        max_spectrs = np.array([], dtype=np.float32)
        while stop < orig_signal.shape[0]:
            signal = orig_signal[start:stop]
            F, freq = self.utils.fft_on_signal(signal, self.fps)

            # position of low and high frequency border (30 and 120 pulse)
            low_index = (np.abs(freq[:signal_len/2] - 0.5)).argmin()
            high_index = (np.abs(freq[:signal_len/2] - 2)).argmin()
            max_index = abs(F[low_index:high_index]).argmax() + low_index
            bpm = freq[max_index] * 60.
            max_spectrs = np.append(max_spectrs, bpm)
            
            if show_window_detail:
                print 'max-index: ', `max_index`
                print 'frekvence: ', `freq[max_index]`
                print 'SPEKTRUM-CUT-max pulse: {0}'.format(bpm)
                self.graphs.signal_1ch_fft(signal, 30)

            start += step
            stop += step
        print '| Spektrum WINdow MEAN:  | {0:.2f}'.format(np.mean(max_spectrs))
        print '|----------------------------------'
        print '| Spektrum WINdow MEDIAN:| {0:.2f}'.format(np.median(max_spectrs))
        print '|----------------------------------'
        print 'max specters: ', `max_spectrs`
        bpm = np.median(max_spectrs)

        print '|----------------------------------|'
        print '|----------------------------------|'

        #now returns last bmp -from specters
        return bpm
