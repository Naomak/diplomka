import cv2
from cv2 import VideoCapture

import numpy as np

from utils import Utils
from utils.exceptions import ThesisException
from graphs import Graphs

class MeanFace():
    # >:( mean, right?
    '''Detect face roi and count mean values from its subparts.
    Two possible interactions:
        - load_from_file - obvious action (*.npy)
        - pass/load video and processe it
    Result is stored in self.roi_mean_signal.
    '''


    def __init__(self):
        self.video = None
        self.roi_mean_signal = None     # for storing counted mean signal
        self.small_roi_mean_signal = None     # for storing counted mean signal
        self.haar_path = '/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml'
        self.save = None
        self.save_file = ''
        self.utils = Utils()
        self.graphs = Graphs()

    # Visualise detected face and ROI
    # def draw_rects(self, img, rects, color):
    #     def get_face_rois(x1, y1, x2, y2):
    #         """From face roi get one roi on forehead and second between eyes and mouth.
    #         Position is given like: left-top and right-bottom.
    #         """
    #         delta_x = abs(x2-x1)
    #         delta_y = abs(y2-y1)

    #         # resize to 20%
    #         delta_x = int(delta_x * 0.2)

    #         delta_y_10 = int(delta_y * 0.1)
    #         delta_y_30 = int(delta_y * 0.3)
    #         roi_forehead = (x1 + delta_x, y1 + delta_y_10), (x2 - delta_x, y1 + delta_y_30)

    #         delta_y_50 = int(delta_y * 0.5)
    #         roi_nose = (x1 + delta_x, y1 + delta_y_50), (x2 - delta_x, y2 - delta_y_30)

    #         return roi_forehead, roi_nose

    #     for x1, y1, x2, y2 in rects:
    #         print x1, y1, x2, y2
    #         cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)
    #         roi_forehead, roi_nose = get_face_rois(x1, y1, x2, y2)
    #         cv2.rectangle(img, roi_forehead[0], roi_forehead[1], color, 2)
    #         cv2.rectangle(img, roi_nose[0], roi_nose[1], color, 2)

    def plot_mean_signal(self, fps):
        if self.small_roi_mean_signal is not None:
            self.graphs.show_signal_1ch(self.small_roi_mean_signal[:,1], fps)
        else:
            print '[MeanFace] Nothing to plot..'


    def load_from_file(self, data_file):
        '''No computing, just reading formerly computed mean signal.'''
        self.roi_mean_signal = np.load(data_file)
        return self.roi_mean_signal

    def compute_roi_mean_signal(self, channel=1, video_file=None, video=None,
            color_type='bgr', save=False, save_file='./roi_mean.npy',low_video=None):
        '''Load video, detects faces and count mean on roi subareas.

        Return counted mean signal.
        Parameters:
        video_file  |\  Path to video file to detect from OR
                    |}-
        video       |/  Already loaded video to detect from
        color_type  - color type of passed video (ycc/bgr),
        save        - flag if mean signal should be saved
        save_file   - file for saving mean signal

        low_video
        '''

        self.color_type = color_type
        self.save = save
        self.save_file = save_file
        self.low_video = low_video

        # load video into private variable self.video
        self.load_video(video_file, video)

        # face detection + specify roi subareas
        # count mean and create mean signal
        self.face_detect_and_mean()

        return self.small_roi_mean_signal
  
    def  load_video(self, video_file, video):
        '''Either stores passed video, or load video from file.'''

        if video is not None:
            self.video = video
        else:
            video_cap = VideoCapture(video_file)
            if not video_cap.isOpened():
                raise ThesisException(
                    'Could not open input file {0}'.format(video_file) )

            fps = int(video_cap.get(cv2.cv.CV_CAP_PROP_FPS))
            width = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
            height = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
            frame_cnt = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))

            self.video = np.ndarray((frame_cnt, height, width, 3), dtype=np.uint8)

            #beginning only trash
            for i in xrange(frame_cnt):
                _, img = video_cap.read()

                np.copyto(self.video[i], img)

            video_cap.release()

    def face_detect_and_mean(self):
        '''
        Convert to gray, detect faces. Set sub-roi from face-roi and count mean from sub-roi.
        Return mean signal for every channel. 
        '''

        def detect(img, cascade):
            # interestingly - slower parameters here produce nicer mean signal in final
            rects = cascade.detectMultiScale(img, scaleFactor=1.4, minNeighbors=4, minSize=(70, 70), flags = cv2.CASCADE_SCALE_IMAGE)
            if len(rects) == 0:
                return []
            rects[:, 2:] += rects[:, :2]
            return rects

        def get_face_rois(x1, y1, x2, y2):
            """From face roi get one roi on forehead and second between eyes and mouth.
            Position is given like: left-top and right-bottom.
            """
            delta_x = abs(x2-x1)
            delta_y = abs(y2-y1)

            # resize to 20%
            delta_x = int(delta_x * 0.2)

            delta_y_10 = int(delta_y * 0.1)
            delta_y_30 = int(delta_y * 0.3)
            roi_forehead = (x1 + delta_x, y1 + delta_y_10), (x2 - delta_x, y1 + delta_y_30)

            delta_y_50 = int(delta_y * 0.5)
            roi_nose = (x1 + delta_x, y1 + delta_y_50), (x2 - delta_x, y2 - delta_y_30)

            return roi_forehead, roi_nose
        ########################################################################

        print 'Converting video to gray.'
        gray_video = self.utils.convert_video(self.video, convert_from=self.color_type, convert_to='gray')
        print '..done'

        # --- FACE DETECTION ---
        face_cascade = cv2.CascadeClassifier(self.haar_path)

        self.roi_mean_signal = np.ndarray( (self.video.shape[0], 3), dtype=np.float32)
        self.small_roi_mean_signal = np.ndarray( (self.video.shape[0], 3), dtype=np.float32)
        print 'Detecting faces and creating mean signal.'
        i = 0
        enth = 3
        rects_detected = None
        rects_used = None
        for img in gray_video:
            # detection on gray images
            if i % enth == 0:
                # print ' detection..'
                rects_detected = detect(img, face_cascade)

                # Check if face was detected
                if rects_detected != []:
                    rects_used = rects_detected
                else:
                    # if nothing was detected, use previous values, if it was first frame, set mean to 0
                    if rects_used is None:
                        self.roi_mean_signal[i] = 0
                        continue
                    # else rects_used stay the same

                x1, y1, x2, y2 = rects_used[0]
                roi_forehead, roi_nose = get_face_rois(x1, y1, x2, y2)


            # count mean of signal channels in roi
            area_mean = tuple(sum(t) for t in zip(
                            self.utils.get_area_mean(roi_forehead, self.video[i]), 
                            self.utils.get_area_mean(roi_nose, self.video[i])
                            )
                        )

            area_mean = ( map(lambda(x): x / 2., area_mean) )   # to keep mean, because of summing two topples above
            arr = np.array(area_mean, dtype=np.float32)
            area_mean = cv2.convertScaleAbs(arr)
            area_mean = area_mean.reshape(1, 3)
            np.copyto(self.roi_mean_signal[i], area_mean)
            
            small_area_mean = tuple(sum(t) for t in zip(
                    self.utils.get_smaller_area_mean(roi_forehead, self.low_video[i]), 
                    self.utils.get_smaller_area_mean(roi_nose, self.low_video[i])
                    )
                )
            small_area_mean = ( map(lambda(x): x / 2., small_area_mean) )
            # create numpy array from list
            small_area_mean = np.array(small_area_mean, dtype=np.float32)
            # np.clip(small_arr, 0, 255, out=small_arr)
            # small_area_mean = small_arr.astype(np.uint8)

            small_area_mean = small_area_mean.reshape(1, 3)
            np.copyto(self.small_roi_mean_signal[i], small_area_mean)

            # Show detected face
            # self.draw_rects(img, rects_used, (0, 255, 0))
            # cv2.imshow('facedetect', img)
            # cv2.imwrite('/home/goffrey/Dropbox/Diplomka/code/plakat/face_roi.jpg', img)
            # cv2.waitKey(33)
    
            i += 1

        if self.save:
            print 'Saving roi mean signal into "{0}"'.format(self.save_file)
            np.save(self.save_file, self.roi_mean_signal)

            name_small = self.save_file[:-4] + '_small.npy'
            print 'Saving SMALL roi mean signal into "{0}"'.format(name_small)
            np.save(name_small, self.small_roi_mean_signal)
            

        # cv2.destroyAllWindows()

        print '--- Face detection DONE ---'
