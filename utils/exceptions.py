
class ThesisException(Exception):
    def __init__(self, description, value=None):
        self.description = description
        self.value = value
    def __str__(self):
        out_str = 'ERROR: ' + `self.description`
        if self.value:
            out_str += '  with value: ' + `self.value`

        return out_str
        