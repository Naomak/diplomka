
import warnings

import cv2
import numpy as np
from numpy import ComplexWarning
from scipy import fftpack, log10

from graphs import Graphs
from utils import Utils
from globs import GLOBS 


class HeatMap():
    '''
    Manages creating heat maps from video by specified pulse value.
    '''
    def __init__(self):
        self.video = None    # modified video (filtered and amplified)
        self.video_format = None    # video format of processed video (bgr/ycc)
        self.fps = 0
        self.heat_map_pulse = 0.0
        self.utils = Utils()
        self.graphs = Graphs()

    def prepare(self, video, video_format, fps, heat_map_pulse, params, output_base_name='output'):
        self.video = video    # modified video (filtered and amplified)
        self.video_format = video_format    # video format of processed video (bgr/ycc)
        self.fps = fps
        self.heat_map_pulse = heat_map_pulse
        self.output_base_name = output_base_name
        self.params = params


    def create_heat_map(self):
        def scale_img(img, max_value):
            ''' Scale to eange 0- max_value'''
            high_index = np.unravel_index(img.argmax(), img.shape)
            max_found = img[high_index]
            low_index = np.unravel_index(img.argmin(), img.shape)
            min_found = img[low_index]

            actual_range = max_found - min_found
            step = float(max_value) / actual_range

            scaled_img = (img - min_found) * step
            return cv2.convertScaleAbs(scaled_img)

        def show_small_big(img, method):
            orig_img = self.utils.reconstruct_image(img, 4, (640, 480))
            cv2.imshow('Metoda: ' + `method`, img)            
            cv2.imshow('Orig: '+ `method`, orig_img)

        def show_img(img, method):
            cv2.imshow('Metoda: ' + `method`, img)   

        def save_img(img, method):
            file_name = '../output/heat_map_test/hm_{0}_p-{2}_m-{1}_{3}.png'.format(self.output_base_name, method, self.heat_map_pulse, self.params)
            cv2.imwrite(file_name, img)
            

        ONE = False
        TWO = False
        THREE = True
        FOUR = False
        FIVE = False
        SIX = False

        f_low = (self.heat_map_pulse - 2) / 60.0
        f_high = (self.heat_map_pulse + 2) / 60.0
        f_ideal = self.heat_map_pulse / 60.0
        print 'HEAT_MAP', `self.heat_map_pulse`
        print 'f_low', `f_low`
        print 'f_high', `f_high`

        video_len = self.video.shape[0]
        F, frequencies = self.utils.fft_on_video(self.video, self.fps)

        channel = 0 if self.video_format == 'ycc' else 1
        

        # for method in range(5):
        for method in [0,1]:
            if method == 4:
                # SIX
                '''Max value is searched in face ROI.
                kde najdu max value, to spektrum beru jako vychozi
                '''
                print 'Try SIX'
                

                top_roi = log10( abs(F[:, 6:10, 17:26, channel] * 2 / video_len))
                bottom_roi = log10( abs(F[:, 14:20, 17:26, channel] * 2 / video_len))
                roi = np.ndarray( (F.shape[0], 10, 9), dtype=top_roi.dtype )
                roi[:, :4, :] = top_roi[:, :, :]
                roi[:, 4:, :] = bottom_roi[:, :, :]

                pos_ideal = (np.abs(frequencies[:video_len/2] - f_ideal)).argmin()

                roi_cut = roi[pos_ideal-2:pos_ideal+2, :, :]
                F_cut = log10( abs(F[pos_ideal-2:pos_ideal+2, :, :, channel] * 2 / video_len))

                # select position by ONE maximal value from face ROI, not by maximal sum of spektrum for one pixel 
                max_index = np.unravel_index(roi_cut.argmax(), roi_cut.shape)
                roi_spectrum_sums = np.sum(roi_cut, axis=0)
                reference_max = roi_spectrum_sums[max_index[1], max_index[2]]

                spectrum_sums = np.sum(F_cut, axis=0)
                dif_img = np.abs(reference_max - spectrum_sums)

                # scale differencies to 0-255
                img = scale_img(dif_img, 255.0)

                # show small and big iamge reconstructed from small
                # show_small_big(img, method)
                orig_img = self.utils.reconstruct_image(img, 4, (640, 480))
                show_img(orig_img, method)
                save_img(orig_img, method)

            elif method == 3:
                # FIVE
                ''' Like 3, but not only one max value, but surrounding of max value.
                Finding max from all spektrums (pixels).
                '''
                print 'Try FIVE'
                
                pos_ideal = (np.abs(frequencies[:video_len/2] - f_ideal)).argmin()
                # select surrounding of desired frequency
                F_cut = log10( abs(F[pos_ideal-2:pos_ideal+2, :, :, channel] * 2 / video_len))

                # select position by ONE maximal value, not by maximal sum of spektrum for one pixel 
                max_index = np.unravel_index(F_cut.argmax(), F_cut.shape)
                spectrum_sums = np.sum(F_cut, axis=0)
                reference_max = spectrum_sums[max_index[1], max_index[2]]
                dif_img = np.abs(spectrum_sums - reference_max)

                # scale differencies to 0-255
                img = scale_img(dif_img, 255.0)

                # show small and big iamge reconstructed from small

                # show_small_big(img, method)
                # show_small_big(img, method)
                orig_img = self.utils.reconstruct_image(img, 4, (640, 480))
                show_img(orig_img, method)
                save_img(orig_img, method)

            elif method == 2:
                # FOUR
                '''Vytvoreni jednoho promerneho spektra v oblasti detekovane ROI.'''
                print 'Try FOUR'


                top_roi = log10( abs(F[:, 6:10, 17:26, channel] * 2 / video_len))
                bottom_roi = log10( abs(F[:, 14:20, 17:26, channel] * 2 / video_len))

                top_mean = np.mean(top_roi, axis=(1, 2) )
                bottom_mean = np.mean(bottom_roi, axis=(1, 2))
                F_mean = (top_mean + bottom_mean) / 2

                F_img = log10( abs(F[12, :, :, channel] * 2 / video_len))
                # 0.795 zde odpovide nejvyssi hodnote na F_mean? cili F[12]?
                dif_img = np.abs(F_img - 0.795)

                # scale differencies to 0-255
                img = scale_img(dif_img, 255.0)
                # show small and big iamge reconstructed from small
                # show_small_big(img, method)
                orig_img = self.utils.reconstruct_image(img, 4, (640, 480))
                show_img(orig_img, method)
                save_img(orig_img, method)


            elif method == 1:
                # THREE
                print 'Try THREE'
                
                pos_ideal = (np.abs(frequencies[:video_len/2] - f_ideal)).argmin()
                F_img = log10( abs(F[pos_ideal, :, :, channel] * 2 / video_len))
                high_index = np.unravel_index(F_img.argmax(), F_img.shape)
                max_value = F_img[high_index]
                dif_img = np.abs(F_img - max_value)

                # scale differencies to 0-255
                img = scale_img(dif_img, 255.0)

                # show small and big image reconstructed from small
                # show_small_big(img, method)
                orig_img = self.utils.reconstruct_image(img, 4, (640, 480))
                show_img(orig_img, method)
                save_img(orig_img, method)

                

            elif method == 0:
                #ONE
                print 'Try ONE'
                filtered = self.utils.temporal_ideal_bandfilter(self.video, self.fps, f_low, f_high)

                difference = self.video - filtered
       
                single_channel = np.ndarray( (self.video.shape[0], self.video.shape[1], self.video.shape[2]), dtype=np.float32 )
                single_channel = difference[:, :, :, channel]

                result = np.sum(np.absolute(single_channel), axis=0)

                # scale differencies to 0-255
                img = scale_img(result, 255.0)

                # show small and big image reconstructed from small
                # show_small_big(img, method)
                orig_img = self.utils.reconstruct_image(img, 4, (640, 480))
                show_img(orig_img, method)
                save_img(orig_img, method)

        cv2.waitKey(0)
        cv2.destroyAllWindows()
