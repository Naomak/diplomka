import cv2
from cv2 import VideoCapture
import numpy as np

from scipy import log10
from scipy import fftpack

from exceptions import ThesisException

'''
TODO:
    - urychleni fft zvetsenim delky signalu na nasobek dvou

NOTES:
    - mozny problem ve filtrovani frekvence pri indexu freqence 0 nebo 1 (v naslednem slicingu pole)
'''


class UtilException(ThesisException):
    def __str__(self):
        return '[UTILS] ' + super(UtilException, self).__str__()


class Utils():
    def capture_video(self, file_path):
        '''
        Simple method to read data from camera and stores it to file.
        Start to capture on keypress 'c'.
        Shut down on keypress 'q'.
        '''
        video_cap = cv2.VideoCapture(0)
        if not video_cap.isOpened():
            raise UtilException('Cannot read form camera')

        width = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        height = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
        fps = 25

        print '::CAMERA::'
        print 'fps: ', `fps`
        print 'width: ', `width`
        print 'height: ', `height`

        

        fourcc = cv2.cv.CV_FOURCC('M', 'J', 'P', 'G')
        vid_writer = cv2.VideoWriter(file_path, fourcc, fps, (width, height), isColor=True)


        print 'Save to: ', file_path

        write_flag = False
        f = 0
        while(video_cap.isOpened()):
            ret, frame = video_cap.read()
            if ret==True:
                # print f
                # if cv2.waitKey(1) & 0xFF == ord('c'):
                    # print 'START capture'
                    # write_flag = True
                # write the flipped frame
                # if write_flag:
                vid_writer.write(frame)
                f+=1
                if f == 100:
                    print 100
                if f == 300:
                    print 300
                if f == 500:
                    break

                # cv2.imshow('Webcamera',frame)
                

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    print 'STOPPING'
                    break
            else:
                break


        # Release everything if job is finished
        video_cap.release()
        vid_writer.release()
        cv2.destroyAllWindows()

    def convert_video(self, video, convert_from='bgr', convert_to='gray'):
        """Return converted video into specified format.
        So far suppose video is converted to some format from BGR.

        Convert from and convert to are two lists  MA TO CENU DELAT DVE KONVERZE? NENI LEPSI UDELAT JEDNU DO FINALNIHO FORMATU?
        """

        converted = None            # output converted video
        convert_format = None
        trans_format = None
        transition = False

        if convert_to == 'gray':
            converted = np.ndarray((video.shape[0], video.shape[1], video.shape[2]), dtype=np.uint8)
            if convert_from != 'bgr' and convert_from != '':
                transition = True
                
            if convert_from == 'ycc':
                trans_format = cv2.cv.CV_YCrCb2BGR

            convert_format = cv2.cv.CV_BGR2GRAY
        


        else:
            converted = np.empty_like(video)
            if (convert_from == 'bgr' or convert_from == '') and convert_to == 'ycc':
                convert_format = cv2.cv.CV_BGR2YCrCb
            elif convert_from == 'ycc' and (convert_to == 'bgr' or convert_to == ''):
                convert_format = cv2.cv.CV_YCrCb2BGR
            else:
                raise UtilException('convert_video: Inappropriate convert format')

        i = 0
        for img in video:
            if transition:
                img = cv2.cvtColor(img, trans_format)


            np.copyto(converted[i], cv2.cvtColor(img, convert_format))
            i += 1

        return converted

    def reconstruct_image(self, img, level, desired_size):
        '''

        Params:
        img             - input small image
        level           - what level of pyramid it acatually is
        desired_size    - tuple of (rows, columns)
        '''
        # import ipdb; ipdb.set_trace();
        for l in xrange(level):
            img = cv2.pyrUp(img)

        # @ resize it to fit orig frame
        if img.shape[0] != desired_size[0] or img.shape[1] != desired_size[1]:
            img = cv2.resize(img, desired_size)

        return img

    def get_pyramid_stack(self, video, level):
        """
        Get stack of all images in self.video using gaussian pyramid until 'level'
        Do conversion from BGR to YCC
        TODO:
            - dynamicky menit shape pyramid_stack
            - jine druhy pyramidy (Laplacian)
        """
        print 'Create gauss stack.'
        frame_cnt = video.shape[0]
        height = video.shape[1]
        width = video.shape[2]

        final_width = width / (2**level)
        final_height = height / (2**level)
        pyramid_stack = np.zeros((frame_cnt, final_height, final_width, 3), dtype=np.float32)
        img = np.ndarray(video[0].shape, dtype=np.float32)
        for i in xrange(frame_cnt):
            np.copyto(img, video[i])
            g_img = self.gaussian_pyramide_at_lvl(img, level)
            np.copyto(pyramid_stack[i], g_img)

        print '..done.'
        return pyramid_stack

    def gaussian_pyramide_at_lvl(self, img, level):
        """Compute gaussian pyramid decomposition
        Params:
          level - tolik kolik je levlu, tolikrat se zavola gaus s naslednym podvzorkovanim
        """
        def get_pyramid_at_lvl(img, level):
            dst = cv2.pyrDown(img)
            level -= 1
            if level == 0:
                
                return dst
            else:
                return get_pyramid_at_lvl(dst, level)

        return get_pyramid_at_lvl(img, level)

    def fft_on_signal(self, signal, fps):
        '''Counting spektrum from passed signal - channel, by 0 dimension.'''

        sample_spacing = 1.0 / fps
        fft_len = signal.shape[0]

        # get spectrum
        F = fftpack.fft(signal, n=fft_len, axis=0)
        # get used frequency
        frequencies = fftpack.fftfreq(int(fft_len), d=sample_spacing)

        return F, frequencies

    def fft_on_video(self, video, fps):
        '''Counting spektrum from passed video - for every pixel, by 0 dimension.'''

        sample_spacing = 1.0 / fps
        fft_len = video.shape[0]

        # get spectrum
        F = fftpack.fft(video, n=fft_len, axis=0)
        # get used frequency
        frequencies = fftpack.fftfreq(int(fft_len), d=sample_spacing)

        return F, frequencies


    def temporal_ideal_bandpass(self, img_stack, fps, f_low, f_high):
        sample_spacing = 1.0 / fps



        # NEXT POWER OF 2
        # fft_len = 2**np.ceil(np.log2(img_stack.shape[0]))
        # F = fftpack.fft(signal, n=fft_len, axis=0 )
        fft_len = img_stack.shape[0]

        # get spectrum
        F = fftpack.fft(img_stack, n=fft_len, axis=0 )
        # get used frequency
        freq = fftpack.fftfreq(int(fft_len), d=sample_spacing)

        # look only in first part (second part is same, but only with negative frequencies) 
        f =  freq[:fft_len/2]

        if f_low:
            # looking for indexis of my limiting frequencies
            pos_low = (np.abs(f - f_low)).argmin()
            # masking - all except my band in positive and negative values
            F[:pos_low] = 0
            F[-(pos_low-1):] = 0

        if f_high:
            pos_high = (np.abs(f - f_high)).argmin()
            F[pos_high:-(pos_high-1)] = 0


        return fftpack.ifft(F, axis=0)

    def temporal_ideal_bandfilter(self, img_stack, fps, f_low, f_high):
        '''
        TODO asi taky checknout rychlost fft na velikosti nasobku dvou jako v bandpass variante
        TODO mozna presneji ochecknout jestli se mazou spravne zaporne frekvence
        '''
        sample_spacing = 1.0 / fps
        fft_len = img_stack.shape[0]

        # get spectrum
        F = fftpack.fft(img_stack, n=fft_len, axis=0 )
        # get used frequency
        freq = fftpack.fftfreq(int(fft_len), d=sample_spacing)

        # look only in first part (second part is same, but only with negative frequencies) 
        f =  freq[:fft_len/2]

        # looking for indexis of my limiting frequencies
        pos_low = (np.abs(f - f_low)).argmin()
        pos_high = (np.abs(f - f_high)).argmin()

        # masking selected band in positive and negative values
        F[pos_low:pos_high] = 0
        # -1 in indexes to let negative values correspond to their positive because
        #of slicing with negative values
        F[-(pos_high-1):-(pos_low-1)] = 0

        return fftpack.ifft(F, axis=0)


    def  get_area_mean(self, roi_pos, img, channels=3):
        """
        Params:
        roi_pos - two tuples, coordinate (x,y) of left-top and right-bottom position
        channels - optional- how many channels to return, for 1 returns middle channel
        """
        m1 = int(np.mean(img[roi_pos[0][1]:roi_pos[1][1], roi_pos[0][0]:roi_pos[1][0], 1]) )
        if channels != 1:
            m0 = int (np.mean(img[roi_pos[0][1]:roi_pos[1][1], roi_pos[0][0]:roi_pos[1][0], 0]) )
        if channels == 3:
            m2 = int (np.mean(img[roi_pos[0][1]:roi_pos[1][1], roi_pos[0][0]:roi_pos[1][0], 2]) )

        if channels == 3:
            return (m0, m1, m2)
        elif channels == 2:
            return (m0, m1)
        elif channels == 1:
            return m1

    def get_smaller_area_mean(self, roi_pos, img):
        div = 2**4
        roi_pos = ((roi_pos[0][0]/div, roi_pos[0][1]/div), (roi_pos[1][0]/div, roi_pos[1][1]/div))

        m0 = np.mean(img[roi_pos[0][1]:roi_pos[1][1]+1, roi_pos[0][0]:roi_pos[1][0]+1, 0]) 
        m1 = np.mean(img[roi_pos[0][1]:roi_pos[1][1]+1, roi_pos[0][0]:roi_pos[1][0]+1, 1]) 
        m2 = np.mean(img[roi_pos[0][1]:roi_pos[1][1]+1, roi_pos[0][0]:roi_pos[1][0]+1, 2]) 

        return (m0, m1, m2)

            
    def save_video(self, video, fps, path):
        fourcc = cv2.cv.CV_FOURCC('M', 'J', 'P', 'G')

        color = True if video.ndim == 4 else False

        vid_writer = cv2.VideoWriter(path, fourcc, fps, (video.shape[1], video.shape[2]), isColor=color)
        for img in video:
            vid_writer.write(img) 

    def do_mask(self, mask, treshold):
        height = mask.shape[0]
        width = mask.shape[1]

        for row in xrange(height):
            for col in xrange(width):
                if mask[row, col].max() > treshold:
                    mask[row, col, :] = treshold
                else:
                    mask[row, col, :] = 0

if __name__ in ('__main__'):
    pass

