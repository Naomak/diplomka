

class GLOBS():
    # celo velka - x:336, y:112
    # lico velka - x:400, y:272
    # mimo velka - x:96, y:320

    # !! POZOR - klasicke koordinaty x a y z leveho horniho rohu
    # 6, 20   # oblast niceho
    # 25, 17  # lico
    # 21, 7    # celo

    # nic
    # vis_x = 6
    # vis_y = 20

    # mikyna
    # vis_x = 13
    # vis_y = 27

    # leve oko
    # vis_x = 18
    # vis_y = 11

    # maximalni hodnota
    # vis_x = 28
    # vis_y = 7

    # celo
    # vis_x = 21
    # vis_y = 7

    # celo 2
    # vis_x = 21
    # vis_y = 9

    # lico
    vis_x = 25
    vis_y = 17
    
    # Valus used during testing
    visual = {
        'fft_from_stack': False,
        'filter_band_pass': False,
        'sig_before_after': False,
        'show_signal_3ch': False,
        'show_pure_magnif_stack': False,
        'show_magnif_signal_stack': False
    }

    state = {
        'face_detection_file_read': False,
        'face_detection_file_save': False,
    }

    action = {
        'do_mask_smallmagniff': False,
        # To control summation original signal with amplified.
        # If False, summation is done diretly
        # If True - Only summation above 255 is passed, otherwise it stays as original signal
        'controled_amplify': False,
    }
    