#!/usr/bin/python2.7

import argparse
import warnings
import sys
import os
import glob     # regexp for finding files

from cv2 import VideoCapture
import cv2
import numpy as np
from numpy import ComplexWarning

from utils.globs import GLOBS
from graphs import Graphs
from utils import Utils
from utils.exceptions import ThesisException
from utils.meanface import MeanFace
from utils.pulsemeasurer import PulseMeasurer
from utils.heatmap import HeatMap


################################################################################
class PulseDetector():
    '''Main class. Drive everything.
    Processing argumentrs loading video and calling other class instances to further process.
    '''
    def __init__(self):

        self.win = 'Window_1'
        self.win_2 = 'Window_1'

        # params
        self.pyr_level = 4        # gauss pyramid level to be used
        self.f_low = 50. / 60       # lower frequency pass
        self.f_high = 1.0      # higher frequency pass  60. / 60.
        self.alpha = 50             # amplifying factor
        self.heat_map_pulse = None
        # convert_to - can be passed as argument to program
        self.convert_to = 'bgr'

        # read video
        self.tmp_bin_path_out = '../output/mean_signal_c-{color}_fps-{fps}_a-{alpha}_f-{f_low}-{f_high}_B.npy'
        self.input_vid = ''
        self.output_vid = ''
        self.width = 0
        self.height = 0
        self.frame_cnt = 0
        self.fps = 30

        self.video = None               # loaded video original data
        self.video_croped = None        # croped version of video
        self.pyramid_stack = None       # smaller version of video
        self.pyramid_stack_croped = None    # croped from beginning and end by cropping size
        # temporary modified video - use to be small pyramid level, filtered and magnified, OR other modifications
        self.modified_video = None      


        # filtered, magnified small pyramid image added to original small image. converted to uint8
        self.small_magnified_video = None 
        self.roi_mean_signal = None            # for saving mean signal from face detected roi area

        # indexes to be set for croping modified and magnified video
        self.side_crop_begin = 0
        self.side_crop_end = 1


        self.args = None        # run arguments of program 

        self.utils = Utils()
        self.graphs = Graphs()

    def parse_args(self):
        """Parse input arguments."""

        def create_ouput_name(output_path, vid_name):

            vid_name = '{name}_a-{alpha}_l-{low:.2}_h-{high:.2}_f-{format}'.format(
                    name= vid_name,
                    alpha = self.alpha,
                    low = self.f_low,
                    high = self.f_high,
                    format = self.convert_to
            )
            out_vid_name = os.path.join(output_path, vid_name)

            out_vid_name = '{vid_name}_{number}.avi'.format(
                vid_name = out_vid_name,
                number = len(glob.glob(out_vid_name + '*' ))
            )

            return out_vid_name

        parser = argparse.ArgumentParser()
        parser.add_argument('--capture-video', help='To capture video from webcam to file specified by parameter.')
        parser.add_argument('-i', '--input-video-path', help='Path to input video.')
        parser.add_argument('-o', '--output-path', help='Destination path for output video.')
        parser.add_argument('-a', '--alpha', help='Amplification factor.', type=int)
        parser.add_argument('-l', '--low-beat', help='Beats per minute, lower bound.', type=float)
        parser.add_argument('-g', '--high-beat', help='Beats per minute, higher bound.', type=float)
        parser.add_argument('-c', '--color-format', help='Color format in which images will be processed (ycc/bgr).')
        parser.add_argument('--output-base-name', help='Base name of output video.')
        parser.add_argument('-m', '--heat-map-pulse', help='Frequncy for heat map pass band filter', type=float)

        parser.add_argument('-p', '--measure-pulse', help='Set, if want to measure pulse from video', action='store_true', default=False)

        parser.add_argument('--pulse-from-video-file', help='Set, if want to measure pulse directly from input video')
        parser.add_argument('--pulse-from-mean-file', help='Measure pulse from 3 channel mean values stored in *.npy file')
        parser.add_argument('--show-mean-from-file', help='Showing signal of strode mean in *.npy file')

        parser.add_argument('--fps', help='FPS of specified file (automaticaly got from read video).', type=int)
        
        args = parser.parse_args()

        if args.alpha:
            self.alpha = args.alpha
        if args.low_beat:
            self.f_low = args.low_beat / 60.       # lower frequency pass
        if args.high_beat:
            self.f_high = args.high_beat / 60.      # higher frequency pass
        if args.heat_map_pulse:
            self.heat_map_pulse = args.heat_map_pulse
        if args.color_format:
            self.convert_to = args.color_format
        else:
            self.convert_to = 'bgr'
        if args.fps:
            self.fps = args.fps

        output_base_name = ''
        if args.output_base_name:
            output_base_name = args.output_base_name
        else:
            output_base_name = 'out_video'
        self.output_base_name = output_base_name
        if args.output_path:
            self.output_vid = create_ouput_name(args.output_path, output_base_name)

        self.input_video_path = args.input_video_path
        self.measure_pulse_flag = args.measure_pulse

        self.args = args

    def get_vid_params(self, video_cap):
        self.fps = int(video_cap.get(cv2.cv.CV_CAP_PROP_FPS))
        self.width = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        self.height = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
        self.frame_cnt = int(video_cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))

        print 'Video params:'
        print 'FPS: ', `self.fps`
        print 'Width: ', `self.width`
        print 'Height: ', `self.height`
        print 'Frame count: ', `self.frame_cnt`



    def load_convert_video(self, input_vid_path, convert_to='bgr'):
        print 'Reading and loading video.'
        convert_format = None
        if convert_to == 'ycc':
            convert_format = cv2.cv.CV_BGR2YCrCb
        elif convert_to == 'gray':
            convert_format = cv2.COLOR_BGR2GRAY

        video_cap = VideoCapture(input_vid_path)
        if not video_cap.isOpened():
            raise ThesisException(
                'Could not open input file {0}'.format(input_vid_path) )

        
        self.get_vid_params(video_cap)

        # crop video at the biginning and at the end by 1 and half second
        # reading frames
        self.side_crop_begin = self.fps + self.fps / 2
        self.side_crop_end = self.frame_cnt - self.side_crop_begin

        # alocation proper size for array
        if convert_to == 'gray':
            self.video = np.ndarray((self.frame_cnt, self.height, self.width), dtype=np.uint8)
        else:
            self.video = np.ndarray((self.frame_cnt, self.height, self.width, 3), dtype=np.uint8)



        # reading frames
        for i in xrange(self.frame_cnt):
            _, img = video_cap.read()
            if convert_to != 'bgr':
                img = cv2.cvtColor(img, convert_format)

            self.video[i, :, :] = img

        video_cap.release()

        # creatig croped version - croped by one and half second from side
        self.video_croped = self.video[self.side_crop_begin:self.side_crop_end]

        print '..done'



    def filter_amplify(self):
        '''Filter with pass band in frequency domain and magnify result with alfa.
        '''
        def filtered_sum(a,b):
            res = np.empty_like(a)
            for x in xrange(a.shape[0]):
                for y in xrange(a.shape[1]):
                    tmp = a[x,y] + b[x,y]
                    bool_check = tmp <= 255
                    if np.all(bool_check):
                        res[x,y,:] = tmp
                    else:
                        res[x,y,:] = a[x,y,:]

            return res

        print '__DO STUFF__'
        
        # format of images to be processed, if original (BGR), set to ''

        frameWait = int(1000 / self.fps)

        # get Gaus/Laplacian pyramid from source video
        self.pyramid_stack = self.utils.get_pyramid_stack(self.video, self.pyr_level)
        self.pyramid_stack_croped = self.pyramid_stack[self.side_crop_begin:self.side_crop_end]

        #-----------------------------------------------------------------------
        # VISUALIZATION - print graph for particular pixel position in time
        if GLOBS.visual['fft_from_stack']:
            signal_type = self.convert_to.upper()
            self.graphs.single_fft_from_img_stack(self.pyramid_stack, self.fps, GLOBS.vis_x, GLOBS.vis_y, signal_type=signal_type)   
            return

        if GLOBS.visual['filter_band_pass']:
            signal_type = self.convert_to.upper()
            # pass filter
            self.graphs.filter_band_pass_signal(self.pyramid_stack, self.fps, GLOBS.vis_x, GLOBS.vis_y, signal_type=signal_type, f_low=0.5, f_high=1.6) 
            return
        #-----------------------------------------------------------------------

        print 'FFT - Temporal filtering'
        filtered = self.utils.temporal_ideal_bandpass(self.pyramid_stack, self.fps, self.f_low, self.f_high)
        print '..done.'
        print 'Amplifying'
        filtered *= self.alpha
        print '..done.'

        # main point is clipping all negative values to be 0
        np.clip(filtered, 0, 255*self.alpha, out=filtered)

        croped_shape = (filtered.shape[0] - 2 * self.side_crop_begin, filtered.shape[1], filtered.shape[2], filtered.shape[3])
        self.small_magnified_video = np.empty(croped_shape, dtype=np.float32)
        self.modified_video = np.empty(croped_shape, dtype=np.float32)

        # internal save
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", ComplexWarning)
            np.copyto(self.modified_video, filtered[self.side_crop_begin:self.side_crop_end], casting='unsafe')
            # np.copyto(img, filtered[i], casting='unsafe')

        if GLOBS.visual['show_pure_magnif_stack']:
            signal_type = self.convert_to.upper()
            self.graphs.single_fft_from_img_stack(self.modified_video, self.fps, GLOBS.vis_x, GLOBS.vis_y, signal_type=signal_type)
            return

        
        img = np.empty_like(self.modified_video[0])
        img_uint = np.empty_like(self.modified_video[0], dtype=np.uint8)


        if GLOBS.action['do_mask_smallmagniff']:
            mask = np.empty_like(self.modified_video[0], dtype=np.float32)
        

        for i in xrange(self.modified_video.shape[0]):
            np.copyto(img, self.modified_video[i])

            if GLOBS.action['controled_amplify']:
                # saturate too high values
                img = filtered_sum(self.pyramid_stack_croped[i], img)
            else:
                img += self.pyramid_stack_croped[i]



            # show mask of images with too strong magnification
            if GLOBS.action['do_mask_smallmagniff']:
                np.copyto(mask, img)
                self.utils.do_mask(mask, 255)
                np.copyto(img_uint, np.clip(img, 0, 255), casting='unsafe')
                cv2.imshow('mask', mask)
                cv2.imshow('mask_big', self.utils.reconstruct_image(mask, self.pyr_level, (self.width, self.height)))
                cv2.imshow('Win_uint', img_uint)
                cv2.imshow('Win_big_uint', self.utils.reconstruct_image(img_uint, self.pyr_level, (self.width, self.height)))
                cv2.waitKey(frameWait)


            np.copyto(self.small_magnified_video[i], img)

        if GLOBS.visual['show_magnif_signal_stack']:
            # POZN: signal_type - zalezi jaka je nastavena konverze
            signal_type = self.convert_to.upper()
            self.graphs.single_fft_from_img_stack(self.small_magnified_video, self.fps, GLOBS.vis_x, GLOBS.vis_y, signal_type=signal_type)    # oblast niceho
            return



    ############################################################################
    def pyramid_merge_save(self, filtered, converted_to='', pyr_video=None, do_save=True):
        """
        Reconstruct pyramid from modified and merge it with original value. For showing magnification effect. 
        Parameters:
          filtered - already filtered (space, temporal) stack of pyramid images
          video_cap  - capture to original video
          converted_to - to specialy specify output video format
          pyr_video - optional, used only in graph visualization

          TODO:
        """
        print 'Merge original with amplified'

        frameWait = int(1000 / self.fps)

        if do_save:
            fourcc = cv2.cv.CV_FOURCC('M', 'J', 'P', 'G')
            vid_writer = cv2.VideoWriter(self.output_vid, fourcc, self.fps, (self.width, self.height), isColor=True)


        img = np.empty_like(self.video[0], dtype=np.float32)

        img_uint = np.empty_like(self.video[0])

        # for every frame from croped orig vid 
        for i in xrange(self.video_croped.shape[0]):
            np.copyto(img, self.video_croped[i])


            # new copy and cast complex to float
            # recreation, becaise it gonna grow to original size
            pyr_img = np.empty_like(filtered[0])
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", ComplexWarning)
                np.copyto(pyr_img, filtered[i], casting='unsafe')

            pyr_img = self.utils.reconstruct_image(pyr_img, self.pyr_level, img.shape)

            img += pyr_img

            np.clip(img, 0, 255, out=img)
            
            np.copyto(img_uint, img, casting='unsafe')


            if do_save:
                # @ convert to bgr nebo v cem to zapisuje
                if converted_to:
                    converted_format = None
                    if converted_to == 'ycc':
                        converted_format = cv2.cv.CV_YCrCb2BGR
                        img_uint = cv2.cvtColor(img_uint, converted_format)
                    
                # @ save into output array
                vid_writer.write(img_uint)

        print 'Write output video into: ', self.output_vid
        print '..done.' 


############################################################################
                     ###   The main body of program.    ###
############################################################################
if __name__ in ('__main__'):
    pulse_detector = PulseDetector()
    pulse_detector.parse_args()

    measure_pulse = None
    try:
        if pulse_detector.args.capture_video:
            pulse_detector.utils.capture_video(pulse_detector.args.capture_video)

        # --- just GRAPHS showing ---
        elif pulse_detector.args.show_mean_from_file:
            mean_face = MeanFace()
            mean_face.load_from_file(
                pulse_detector.args.show_mean_from_file
            )
            mean_face.plot_mean_signal(30)

        # --- COMPUTING ---
        else:
            measure_pulse_flag = True

            # read video and count mean faces directly from that
            if pulse_detector.args.pulse_from_video_file:
                mean_face = MeanFace()
                pulse_detector.roi_mean_signal = mean_face.compute_roi_mean_signal(
                    video_file=pulse_detector.args.pulse_from_video_file,
                    color_type=pulse_detector.convert_to,
                    save=True,
                    save_file=pulse_detector.tmp_bin_path_out.format(pulse_detector.convert_to),
                )
            
            # already counted, only read and store from mean *.npy file
            elif pulse_detector.args.pulse_from_mean_file:
                # measure pulse
                mean_face = MeanFace()
                pulse_detector.roi_mean_signal = mean_face.load_from_file(
                    pulse_detector.args.pulse_from_mean_file
                )

            # First process input video, then could continue with face detection and mean
            else:            
                pulse_detector.load_convert_video(pulse_detector.input_video_path, convert_to=pulse_detector.convert_to)

                # filter and amlify given frequencies
                pulse_detector.filter_amplify()
                # write down amplified video
                pulse_detector.pyramid_merge_save(pulse_detector.modified_video, converted_to=pulse_detector.convert_to, do_save=False)

                # pulse_detector.miracle()

                # want to measure pulse from just created video - prepare measurer object
                if pulse_detector.args.measure_pulse:
                    mean_face = MeanFace()
                    pulse_detector.roi_mean_signal = mean_face.compute_roi_mean_signal(
                        video=pulse_detector.video_croped,
                        color_type=pulse_detector.convert_to,
                        save=True,
                        save_file=pulse_detector.tmp_bin_path_out.format(
                                    color=pulse_detector.convert_to,
                                    fps=pulse_detector.fps,
                                    alpha=pulse_detector.alpha,
                                    f_low=pulse_detector.args.low_beat,
                                    f_high=pulse_detector.args.high_beat ),
                        low_video=pulse_detector.small_magnified_video
                    )
                    mean_face.plot_mean_signal(30)
                    
                else:
                    measure_pulse_flag = False

            # Measure pulse from ROI mean signal (3 channels)
            if measure_pulse_flag:
                pulse_measurer = PulseMeasurer()
                peak_detection_window = 1.2    # in seconds
                pulse_detector.heat_map_pulse = pulse_measurer.measure_pulse(pulse_detector.roi_mean_signal, pulse_detector.fps, pulse_detector.convert_to, peak_detection_window)
                

            if pulse_detector.args.heat_map_pulse:
                heat_map = HeatMap()
                # visualize heat map of frequency change for filtered frequency
                heat_map.prepare(
                        pulse_detector.small_magnified_video,
                        pulse_detector.convert_to,
                        pulse_detector.fps,
                        pulse_detector.heat_map_pulse,
                        params = 'a-{0}_f-{1:.2}-{2:.2}'.format(pulse_detector.alpha, pulse_detector.f_low, pulse_detector.f_high),
                        output_base_name = pulse_detector.output_base_name,
                )
                heat_map.create_heat_map()


    except ThesisException as e:
        print e

