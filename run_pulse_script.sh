for file in marty2
do
    echo "################### FILE: $file ###################"
    for alpha in  30 
    # for alpha in 10
    do
       echo "========== Starting with ALPHA=$alpha =========="

    ./pulsedetector.py \
    --input-video-path /home/goffrey/Skola/Diplomka/src/testy/plakat/$file.avi \
    --output-path /home/goffrey/Skola/Diplomka/output/testy/plakat/ \
    --output-base-name $file \
    --low-beat 50 --high-beat 70 \
    --alpha $alpha \
    --measure-pulse \
    --heat-map-pulse 59
    done
done