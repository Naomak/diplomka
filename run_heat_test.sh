for file in dom
do
    echo "################### FILE: $file ###################"
    for alpha in  10 30 50
    do
    ./pulsedetector.py \
    --input-video-path /home/goffrey/Skola/Diplomka/src/testy/plakat/$file.avi \
    --output-path /home/goffrey/Skola/Diplomka/output/testy/plakat/ \
    --output-base-name $file \
    --low-beat 65 --high-beat 85 \
    --alpha $alpha \
    --heat-map-pulse 60
    done
done