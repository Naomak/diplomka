
from matplotlib import pyplot as plt
from scipy import fftpack, log10
import numpy as np
import cv2

from utils import Utils

'''
Featured graphs:
    single_fft_from_img_stack
    filter_band_pass_signal
    signal_before_after
    show_signal_3ch
    show_signal_1ch
    show_2_signals
    two_signal_difference
    signal_1ch_fft
    signal_3ch_fft
    signal_3ch_fft_pure
    show_signal_1ch_params
'''

class Graphs():
    def __init__(self):
        self.utils = Utils()

    def single_fft_from_img_stack(self, stack, fps, x, y, signal_type):
        '''VISUALIZATION - print graph for particular pixel position in time.
        Position x and y are indexing from top left corner.
        x is meand to be position on x axis, y on y-axis.
        '''

        stack_len = stack.shape[0]
        t = np.arange(stack_len)
        sec_tics = np.arange(stack_len/fps) * fps

        # get spectrum
        # F = fftpack.fft(stack, n=stack_len, axis=0 )
        # get used frequency
        # freq = fftpack.fftfreq(int(stack_len), d=sample_spacing)

        F, freq = self.utils.fft_on_video(stack, fps)

        signal_title = 'Signal {0}'.format(signal_type.upper())

        # LEFT column
        ax11 = plt.subplot(321)
        plt.title(signal_title)
        plt.xlabel(u'Time')
        plt.ylabel('Kanal 0')
        plt.plot(t, stack[:,y,x,0])
        ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax21 = plt.subplot(323, sharex=ax11)
        plt.xlabel(u'Time')
        plt.ylabel('Kanal 1')
        plt.plot(t, stack[:,y,x,1])
        ax21.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax31 = plt.subplot(325, sharex=ax11)
        plt.xlabel(u'Time')
        plt.ylabel('Kanal 2')
        plt.plot(t, stack[:,y,x,2])
        ax31.set_xticks(sec_tics, minor=False)
        plt.grid(True)
       

        # RIGHT column
        ax12 = plt.subplot(322)
        plt.title('log10(abs(FFT * 2/N))')
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( abs(F[:,y,x,0] * 2 / stack_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax22 = plt.subplot(324, sharex=ax12)
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( abs(F[:,y,x,1] * 2 / stack_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax32 = plt.subplot(326, sharex=ax12)
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( abs(F[:,y,x,2] * 2 / stack_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        plt.show()

    def filter_band_pass_signal(self, stack, fps, x, y, signal_type='bgr', f_low=None, f_high=None):
        '''VISUALIZATION - print graph for particular pixel position in time.
        Position x and y are indexing from top left corner.
        x is meand to be position on x axis, y on y-axis.
        Shows difference in signal for original one and its band-pass version in one figure.
        Shows spectrum for signals in second figure.
        Parameters:
        stack       - video like
        '''
        # import ipdb; ipdb.set_trace();
        signal_type = signal_type.upper()
        sample_spacing = 1.0 / fps
        stack_len = stack.shape[0]
        t = np.arange(stack_len)

        sec_tics = np.arange(stack_len/fps) * fps * 3

        # get spectrum
        F = fftpack.fft(stack, n=stack_len, axis=0 )
        # get used frequency
        freq = fftpack.fftfreq(int(stack_len), d=sample_spacing)

        # FILTERING
        F_orig = np.empty_like(F)
        np.copyto(F_orig, F)

        # lookup only in first part (second part is same, but only with negative frequencies) 
        f =  freq[:stack_len/2]

        if f_low:
            # looking for indexis of my limiting frequencies
            pos_low = (np.abs(f - f_low)).argmin()
            # pos_low = 12
            F[:pos_low] = 0
            F[-pos_low:] = 0

            
        if f_high:
            pos_high = (np.abs(f - f_high)).argmin()
            # pos_high = 16
            F[pos_high:-pos_high] = 0


        # masking - all except my band in positive and negative values

        i_signal = fftpack.ifft(F, axis=0)

        # --- FIGURE 1: signal ------------------------
        # LEFT column - original signal
        plt.figure(1)
        plt.suptitle('{type} signal on top left pos ({x}, {y})'.format(type=signal_type, x=x, y=y))
        ax111 = plt.subplot(321)
        plt.title('Power signal {type}'.format(type=signal_type))
        plt.xlabel(u'Frame')
        plt.ylabel('Kanal 0')
        plt.plot(t, stack[:,y,x,0])
        ax111.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax121 = plt.subplot(323, sharex=ax111)
        plt.xlabel(u'Frame')
        plt.ylabel('Kanal 1')
        plt.plot(t, stack[:,y,x,1])
        ax121.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax131 = plt.subplot(325, sharex=ax111)
        plt.xlabel(u'Frame')
        plt.ylabel('Kanal 2')
        plt.plot(t, stack[:,y,x,2])
        ax131.set_xticks(sec_tics, minor=False)
        plt.grid(True)
       
        # RIGHT column - filtered signal
        ax112 = plt.subplot(322)
        plt.title('Filtered signal pass band - ({low}, {high})'.format(low=f_low, high=f_high))
        plt.xlabel(u'Frame')
        plt.ylabel('Kanal 0')
        plt.plot(t, i_signal[:,y,x,0])
        ax112.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax122 = plt.subplot(324, sharex=ax112)
        plt.xlabel(u'Frame')
        plt.ylabel('Kanal 1')
        plt.plot(t, i_signal[:,y,x,1])
        ax122.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax132 = plt.subplot(326, sharex=ax112)
        plt.xlabel(u'Frame')
        plt.ylabel('Kanal 2')
        plt.plot(t, i_signal[:,y,x,2])
        ax132.set_xticks(sec_tics, minor=False)
        plt.grid(True)

       # --- FIGURE 2: spectral magnitude ------------------
        plt.figure(2)
        plt.suptitle('{type} signal on top left pos ({x}, {y})'.format(type=signal_type, x=x, y=y))
        # LEFT column - spectrum magnitude of original
        ax211 = plt.subplot(321)
        plt.title('Before filtering')
        plt.xlabel(u'frequencies')
        plt.ylabel('log10(abs(FFT * 2/N))')
        plt.plot(freq, log10( abs(F_orig[:,y,x,0] * 2 / stack_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax221 = plt.subplot(323, sharex=ax211)
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( np.abs(F_orig[:,y,x,1] * 2 / stack_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax231 = plt.subplot(325, sharex=ax211)
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( abs(F_orig[:,y,x,2] * 2 / stack_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        # RIGHT column - spectrum magnitude filtered
        ax212 = plt.subplot(322)
        plt.title('After filtering')
        plt.xlabel(u'frequencies')
        plt.ylabel('log10(abs(FFT * 2/N))')
        plt.plot(freq, log10( abs(F[:,y,x,0] * 2 / stack_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax222 = plt.subplot(324, sharex=ax212)
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( abs(F[:,y,x,1] * 2 / stack_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax232 = plt.subplot(326, sharex=ax212)
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( abs(F[:,y,x,2] * 2 / stack_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        plt.show()

    def signal_before_after(self, original, magnified, fps, x, y, signal_type='BGR',f_low=None, f_high=None, alpha=None):
        '''Show two signals, 3 channels.
        Need to pass parameters of processing to put down into titles.
        '''
        frames = np.arange(original.shape[0])
        sec_tics = np.arange(original.shape[0]/fps) * fps

        plt.suptitle('{type} Signal on top left pos ({x}, {y}). Before and after filtration and magnitude'.format(type=signal_type, x=x, y=y))
        # LEFT column - original
        ax11 = plt.subplot(321)
        plt.xlabel('Frames')
        plt.ylabel('Kanal 0')
        ax11.set_xticks(sec_tics, minor=False)
        ax11.xaxis.grid(True, which='major')
        plt.plot(frames, original[:,y, x, 0])

        ax21 = plt.subplot(323, sharex=ax11)
        plt.xlabel('Frames')
        plt.ylabel('Kanal 1')
        ax21.set_xticks(sec_tics, minor=False)
        ax21.xaxis.grid(True, which='major')
        plt.plot(frames, original[:,y, x, 1])

        ax31 = plt.subplot(325, sharex=ax11)
        plt.xlabel('Frames')
        plt.ylabel('Kanal 2')
        ax31.set_xticks(sec_tics, minor=False)
        ax31.xaxis.grid(True, which='major')
        plt.plot(frames, original[:,y, x, 2])

        # RIGHT column - magnified
        ax12 = plt.subplot(322, sharex=ax11)
        plt.title('Frequencies pass band ({f_low}, {f_high}), alpha={alpha}'.format(f_low=f_low, f_high=f_high, alpha=alpha))
        plt.xlabel('Frames')
        plt.ylabel('Kanal 0')
        ax12.set_xticks(sec_tics, minor=False)
        ax12.xaxis.grid(True, which='major')
        plt.plot(frames, magnified[:,y, x, 0])

        ax22 = plt.subplot(324, sharex=ax11)
        plt.xlabel('Frames')
        plt.ylabel('Kanal 1')
        ax22.set_xticks(sec_tics, minor=False)
        ax22.xaxis.grid(True, which='major')
        plt.plot(frames, magnified[:,y, x, 1])

        ax32 = plt.subplot(326, sharex=ax11)
        plt.xlabel('Frames')
        plt.ylabel('Kanal 2')
        ax32.set_xticks(sec_tics, minor=False)
        ax32.xaxis.grid(True, which='major')
        plt.plot(frames, magnified[:,y, x, 2])


        plt.show()

    def show_signal_3ch(self, signal, fps):
        """Simple show signal in graph.
        """

        plt.suptitle('Signal')

        frames = np.arange(signal.shape[0])
        sec_tics = np.arange(signal.shape[0]/fps) * fps

        ax1 = plt.subplot(311)
        plt.xlabel('Frames')
        plt.ylabel('Kanal 0')
        ax1.set_xticks(sec_tics, minor=False)
        ax1.xaxis.grid(True, which='major')
        plt.plot(frames, signal[:,0])

        ax2 = plt.subplot(312, sharex=ax1)
        plt.xlabel('Frames')
        plt.ylabel('Kanal 1')
        ax2.set_xticks(sec_tics, minor=False)
        ax2.xaxis.grid(True, which='major')
        plt.plot(frames, signal[:,1])

        ax3 = plt.subplot(313, sharex=ax1)
        plt.xlabel('Frames')
        plt.ylabel('Kanal 2')
        ax3.set_xticks(sec_tics, minor=False)
        ax3.xaxis.grid(True, which='major')
        plt.plot(frames, signal[:,2])


        plt.show()


    def show_signal_1ch(self, signal, fps):
        """Simple show signal in graph.
        """

        plt.suptitle('Signal')

        frames = np.arange(signal.shape[0])
        sec_tics = np.arange(signal.shape[0]/fps) * fps

        ax1 = plt.subplot(111)
        plt.xlabel('Frames')
        # plt.xlabel()
        ax1.set_xticks(sec_tics, minor=False)
        ax1.xaxis.grid(True, which='major')
        plt.plot(frames, signal)

        plt.show()

    def show_2_signals(self, signal1, signal2, fps):

        plt.suptitle('Mean from green? channel signal in face roi area')

        frames = np.arange(signal1.shape[0])
        sec_tics = np.arange(signal1.shape[0]/fps) * fps

        ax1 = plt.subplot(211)
        plt.title('Mean signal')
        plt.xlabel('Frames')
        # plt.ylabel('Kanal 0')
        ax1.set_xticks(sec_tics, minor=False)
        ax1.xaxis.grid(True, which='major')
        plt.plot(frames, signal1)

        frames = np.arange(signal2.shape[0])
        sec_tics = np.arange(signal2.shape[0]/fps) * fps

        ax2 = plt.subplot(212, sharex=ax1)
        plt.title('Detected peaks')
        plt.xlabel('Frames')
        # plt.ylabel('Kanal 1')
        ax2.set_xticks(sec_tics, minor=False)
        ax2.xaxis.grid(True, which='major')
        plt.plot(frames, signal2)

        plt.show()

    def two_signal_difference(self, signal1, signal2, signal3, fps):
        '''
        Signals of the same length.
        '''

        plt.suptitle('Mean from green? channel signal in face roi area')

        frames = np.arange(signal1.shape[0])
        sec_tics = np.arange(signal1.shape[0]/fps) * fps

        ax1 = plt.subplot(221)
        plt.title('Green channel?')
        plt.xlabel('Frames')
        # plt.ylabel('Kanal 0')
        ax1.set_xticks(sec_tics, minor=False)
        ax1.xaxis.grid(True, which='major')
        plt.plot(frames, signal1)

        # frames = np.arange(signal2.shape[0])
        # sec_tics = np.arange(signal2.shape[0]/fps) * fps

        ax2 = plt.subplot(222, sharex=ax1)
        plt.title('Detected peaks')
        plt.xlabel('Frames')
        # plt.ylabel('Kanal 1')
        ax2.set_xticks(sec_tics, minor=False)
        ax2.xaxis.grid(True, which='major')
        plt.plot(frames, signal2)

        ax3 = plt.subplot(223, sharex=ax1)
        plt.title('Difference')
        plt.xlabel('Frames')
        # plt.ylabel('Kanal 1')
        ax2.set_xticks(sec_tics, minor=False)
        ax2.xaxis.grid(True, which='major')
        plt.plot(frames, signal3)

        ax4 = plt.subplot(224, sharex=ax1)
        plt.title('Difference')
        plt.xlabel('Frames')
        # plt.ylabel('Kanal 1')
        ax2.set_xticks(sec_tics, minor=False)
        ax2.xaxis.grid(True, which='major')
        plt.plot(frames, signal3**2)

        plt.show()

    def signal_1ch_fft(self, signal, fps, signal_type='bgr'):
        signal_len = signal.shape[0]
        t = np.arange(signal_len)

        sec_tics = np.arange(signal_len/fps) * fps

        # get spectrum
        F, freq = self.utils.fft_on_signal(signal, fps)


        signal_title = 'Signal {0}'.format(signal_type.upper())

        # LEFT column
        ax11 = plt.subplot(211)
        plt.title(signal_title)
        plt.xlabel(u'Time')
        plt.ylabel('Channel intensity')
        plt.plot(t, signal)
        ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        # RIGHT column
        ax12 = plt.subplot(212)
        plt.title('log10(abs(FFT * 2/N))')
        plt.xlabel(u'frequencies')
        plt.ylabel('spectrum')
        plt.plot(freq, log10( abs(F * 2 / signal_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        plt.show()


    def signal_3ch_fft(self, signal, fps, signal_type='bgr'):
        '''Show 3 channel signal in one column and next to it its spektrum
        '''
        sample_spacing = 1.0 / fps
        signal_len = signal.shape[0]
        t = np.arange(signal_len)

        sec_tics = np.arange(signal_len/fps) * fps

        # get spectrum
        F = fftpack.fft(signal, n=signal_len, axis=0 )
        # get used frequency
        freq = fftpack.fftfreq(int(signal_len), d=sample_spacing)


        signal_title = 'Signal {0}'.format(signal_type.upper())

        # LEFT column
        ax11 = plt.subplot(321)
        plt.title(signal_title)
        plt.xlabel(u'Time')
        plt.ylabel('Kanal 0')
        plt.plot(t, signal[:,0])
        ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax21 = plt.subplot(323, sharex=ax11)
        plt.xlabel(u'Time')
        plt.ylabel('Kanal 1')
        plt.plot(t, signal[:,1])
        ax21.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax31 = plt.subplot(325, sharex=ax11)
        plt.xlabel(u'Time')
        plt.ylabel('Kanal 2')
        plt.plot(t, signal[:,2])
        ax31.set_xticks(sec_tics, minor=False)
        plt.grid(True)
        

        # RIGHT column
        ax12 = plt.subplot(322)
        plt.title('log10(abs(FFT * 2/N))')
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( abs(F[:,0] * 2 / signal_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax22 = plt.subplot(324, sharex=ax12)
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( abs(F[:,1] * 2 / signal_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax32 = plt.subplot(326, sharex=ax12)
        plt.xlabel(u'frequencies')
        plt.ylabel('Log10 abs-spectrum')
        plt.plot(freq, log10( abs(F[:,2] * 2 / signal_len)) )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        plt.show()

    def signal_3ch_fft_pure(self, signal, fps, signal_type='bgr'):
        '''Show 3 channel signal in one column and next to it its spektrum
        '''
        sample_spacing = 1.0 / fps
        signal_len = signal.shape[0]
        t = np.arange(signal_len)

        sec_tics = np.arange(signal_len/fps) * fps

        # get spectrum
        F = fftpack.fft(signal, n=signal_len, axis=0 )
        # get used frequency
        freq = fftpack.fftfreq(int(signal_len), d=sample_spacing)


        signal_title = 'Signal {0}'.format(signal_type.upper())

        # LEFT column
        ax11 = plt.subplot(321)
        plt.title(signal_title)
        plt.xlabel(u'Time')
        plt.ylabel('Kanal 0')
        plt.plot(t, signal[:,0])
        ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax21 = plt.subplot(323, sharex=ax11)
        plt.xlabel(u'Time')
        plt.ylabel('Kanal 1')
        plt.plot(t, signal[:,1])
        ax21.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax31 = plt.subplot(325, sharex=ax11)
        plt.xlabel(u'Time')
        plt.ylabel('Kanal 2')
        plt.plot(t, signal[:,2])
        ax31.set_xticks(sec_tics, minor=False)
        plt.grid(True)
        

        # RIGHT column
        ax12 = plt.subplot(322)
        plt.title('Pure spectrum')
        plt.xlabel(u'frequencies')
        plt.ylabel('spectrum')
        plt.plot(freq, F[:, 0] )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax22 = plt.subplot(324, sharex=ax12)
        plt.xlabel(u'frequencies')
        plt.ylabel('spectrum')
        plt.plot(freq, F[:, 1] )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        ax32 = plt.subplot(326, sharex=ax12)
        plt.xlabel(u'frequencies')
        plt.ylabel('spectrum')
        plt.plot(freq, F[:, 2] )
        # ax11.set_xticks(sec_tics, minor=False)
        plt.grid(True)

        plt.show()

    def show_signal_1ch_params(self, xaxis, yaxis, title):
        ax = plt.plot(xaxis, yaxis)
        plt.title(title)
        plt.xlabel('y')
        plt.ylabel('x')

        # fps = 30
        # signal_len = len(xaxis)
        # sec_tics = np.arange(signal_len/fps) * fps
        # plt.xticks(sec_tics, minor=False)

        plt.grid(True)
        plt.show()