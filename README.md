#Detekce lidského pulsu z videa
- Diplomová práce, FIT VUT Brno, 2014
- Založená na Euler video magnification - http://people.csail.mit.edu/mrub/vidmag/
- Detekce pulsu z obličeje videa, stačí video o kvalitě 640x480, člověk by se neměl hýbat.
- Vytvoření Heat mapy - vizualizace míst s podobnou frekvencí jako je detekovaná frekvence.